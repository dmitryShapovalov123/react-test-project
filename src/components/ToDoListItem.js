import React from "react";
import PropTypes from "prop-types";
import Button from "./Button";

const ToDoListItem = ({ content, identifier, onDelete, onDone, done }) => (
  <div className="toDoListItem">
    <div className={`toDoListItem__content ${!done && "strikeThrough"}`}>
      {content}
    </div>
    <div>
      <Button name="DEL" onClick={onDelete} identifier={identifier} />
      <Button name="EDIT" />
      <Button name="DONE" onClick={onDone} identifier={identifier} />
    </div>
  </div>
);

ToDoListItem.propTypes = {
  content: PropTypes.string,
  onDelete: PropTypes.func
  // key: PropTypes.number
};

ToDoListItem.defaultProps = {
  content: "",
  onDelete: () => console.log("no callback for toDoListItem")
  // key: 1
};

// StatelessComponent.propTypes = {
//   text: PropTypes.string
// };

// StatelessComponent.defaultProps = {
//   text: "Stateless"
// };

export default ToDoListItem;
