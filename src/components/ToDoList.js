import React, { Component } from "react";
import PropTypes from "prop-types";
import Button from "./Button";
import ToDoListItem from "./ToDoListItem";

class ToDoList extends Component {
//   static propTypes = {

  //   }
  constructor(props) {
    super(props);
    this.addToDoListItem = this.addToDoListItem.bind(this);
    this.state = {
      toDoListItems: []
    };
  }

  getInput = (el) => {
    this.input = el;
  }

  deleteToDoListItem = (identifier) => {
    let { toDoListItems } = this.state;

    toDoListItems = toDoListItems.filter(element => element.id !== identifier);
    this.setState({ toDoListItems });
  }

  editToDoListItem = (identifier) => { identifier; }

  markDoneToDoListItem = (identifier) => {
    let { toDoListItems } = this.state;

    toDoListItems = toDoListItems.map((element) => {
      if (identifier === element.id) { element.done = !element.done; }
      return element;
    });
    this.setState({ toDoListItems });
  }

  addToDoListItem() {
    const { toDoListItems } = this.state;
    const inputValue = this.input.value.trim();

    toDoListItems.push({
      content: inputValue,
      id: inputValue.slice(0, 4) + Math.random(),
      done: true,
      editable: false
    });
    this.setState({ toDoListItems });

    this.input.value = "";
  }


  render() {
    const { toDoListItems } = this.state;
    return (
      <div className="toDoList">
        <div className="toDoList__header">
          <div>TO DO LIST</div>
          <input className="input" type="text" ref={this.getInput} />
          <Button
            name="CREATE"
            onClick={this.addToDoListItem}
          />
        </div>
        {toDoListItems.map(item => (
          <ToDoListItem
            key={item.id}
            content={item.content}
            identifier={item.id}
            onDelete={this.deleteToDoListItem}
            onDone={this.markDoneToDoListItem}
            done={item.done}
          />
        ))}
      </div>
    );
  }
}

export default ToDoList;


// value={inputValue} onChange={e => this.onInputValueChange(e)
