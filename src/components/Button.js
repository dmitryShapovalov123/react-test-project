import React from "react";
import PropTypes from "prop-types";

const Button = ({ name, onClick, identifier }) => (
  <button className="button" onClick={() => onClick(identifier)} type="button">
    {name}
  </button>
);

Button.propTypes = {
  name: PropTypes.string,
  onClick: PropTypes.func
};

Button.defaultProps = {
  name: "Stateless",
  onClick: () => console.log("No prop for onclick in button")
};

export default Button;
