import React, { Component } from "react";

import ToDoListItem from "../components/ToDoListItem";
import ToDoList from "../components/ToDoList";

class StartPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // title: "Welcome to Invoice App!"
    };
  }

  render() {
    // const { title } = this.state;

    return (
      <div className="start-page">
        <ToDoList />
      </div>
    );
  }
}

export default StartPage;
